package es.elhaso.gradha.stackoverflow53343622

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.transition.ChangeBounds
import android.support.transition.Transition
import android.support.transition.TransitionManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnticipateInterpolator
import android.view.animation.AnticipateOvershootInterpolator


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClick(dummy: View) {
        CustomDialog(this).show()
    }
}

private const val ANIMATION_DURATION = 300L

class CustomDialog(context: Context) : AlertDialog(context, R.style.Dialog) {

    private var mVisible = false
    private val mVisibleSet = ConstraintSet()
    private val mHiddenSet = ConstraintSet()
    private lateinit var mClRoot: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog)

        mClRoot = findViewById(R.id.clRoot)!!
        mVisibleSet.clone(mClRoot)
        mVisibleSet.connect(R.id.llDialog, ConstraintSet.BOTTOM,
            ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)

        mHiddenSet.clone(mClRoot)
        mHiddenSet.connect(R.id.llDialog, ConstraintSet.TOP,
            ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)

        mHiddenSet.applyTo(mClRoot)
        mClRoot.postDelayed({
            val transition = ChangeBounds()
            transition.duration = ANIMATION_DURATION
            transition.interpolator = AnticipateOvershootInterpolator()

            TransitionManager.beginDelayedTransition(mClRoot, transition)
            mVisibleSet.applyTo(mClRoot)
            mVisible = true
        }, 0)
    }

    override fun dismiss() {
        if (mVisible) {
            val transition = ChangeBounds()
            transition.duration = ANIMATION_DURATION
            transition.interpolator = AnticipateInterpolator()
            transition.addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    mVisible = false
                    dismiss()
                }

                override fun onTransitionResume(transition: Transition) {}
                override fun onTransitionPause(transition: Transition) {}
                override fun onTransitionCancel(transition: Transition) {}
                override fun onTransitionStart(transition: Transition) {}
            })

            TransitionManager.beginDelayedTransition(mClRoot, transition)
            mHiddenSet.applyTo(mClRoot)
        } else {
            super.dismiss()
        }
    }
}
