# How to make AlertDialog show near edge of the screen

## General

This mini sample project displays how a floating AlertDialog shows up some
space near the edge on Xiaomi Mi8 devices due to their rounded corners. Can
this be avoided?. This was asked as [StackOverflow question
53343622](https://stackoverflow.com/questions/53343622/how-to-make-alertdialog-show-near-edge-of-the-screen).
